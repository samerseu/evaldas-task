		  
console.log('jquery executing');

//===================== user ===================================
const userFormReset = () => {
    console.log('userFormReset triggered');
    $('#email').val('');
	$('#password').val('');
	$('#role option').eq(0).prop('selected', true);    
}


$(document).on('click', '#user_reset_btn', (e) => {
    userFormReset();
});


$(document).on('click', '#user_create_btn', (e) => {
    let email  = $('#email').val();
	let pass  = $('#password').val();
	let role       = $('#role option:selected').val();
    let dataStr =  {
                    'email': email,
                    'pass': pass,                	 
                    'role': role 
            };
    if(email === "") {
        alert('Enter email');
        $('#email').focus();
        return false;
    }
    else if(pass === "") {
        alert('Enter password');
        $('#password').focus();
        return false;
    }
    else if(role === "") {
        alert('Select role');
        $('#role').eq(0).focus();;
        return false;
    } else {
           $.ajax({
		    type: 'POST',
		    url: '/create-user',
		    data: dataStr,
		    success:(data) => {
                if(data.flag == 1) {
                  console.log(`yes- ${data.flag}`);
                  $('#user_response_status').text(data.msg);
                  setTimeout(() => {
                     $('#user_response_status').text(''); 
                     userFormReset();      
                  }, 1500);                 
                }
			    console.log(`resposen: ${JSON.stringify(data)}` );
		    }
	    });
    	console.log(`dataStr: ${dataStr}` );
    }

});


//============================ order===============================

const svgContentRemove = () => {
    $('#svgOne').html('');
    let rectangle = document.getElementById('rect');
    let parent    = rectangle.parentNode;
    parent.removeChild(rectangle);
}

const orderFormReset = () => {
    console.log('orderFormReset triggered');
    $('#order_name').val('');
	$('#door_width').val('');
	$('#door_height').val('');
	$('#color option').eq(0).prop('selected', true);
    $('#side option').eq(0).prop('selected', true);
    $('#moves option').eq(0).prop('selected', true);
    svgContentRemove(); 
}

const isNumberKey = (evt) => {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
    }
    return true;
}


const genSVGRectangle = (width, height, color) => {
   console.log(`Color: ${color}`)
 let svgns = "http://www.w3.org/2000/svg";
 let rect = document.createElementNS( svgns,'rect' );
    rect.setAttributeNS( null,'x', 50);
    rect.setAttributeNS( null,'y', 50);
    rect.setAttributeNS( null,'width', width );
    rect.setAttributeNS( null,'height',height );
    rect.setAttributeNS( null,'fill',  color === '' ? '#0000ff' : color );
    document.getElementById( 'svgOne' ).appendChild(rect);
}


$(document).on('keypress', '#door_width', (e) => {
    console.log(`dw: ${$(e.target).val()}`);
    $(e.target).val($(e.target).val().replace(/[^\d]/, ''));    
});

$(document).on('keypress', '#door_height', (e) => {
    console.log(`dh: ${$(e.target).val()}`);
    $(e.target).val($(e.target).val().replace(/[^\d]/, ''));    
});


$(document).on('keyup', '#door_height', (e) => {
    genSVGRectangle($('#door_width').val() ,$('#door_height').val(), null);
});


$(document).on('change', '#color', (e) => {
    if($('#color option:selected').val() !== "") {
        genSVGRectangle($('#door_width').val() ,$('#door_height').val(), $('#color option:selected').text() );
    }else{
        genSVGRectangle($('#door_width').val() ,$('#door_height').val(), null);     
    }
});


$(document).on('click', '#customer_order_reset_btn', (e) => {
    orderFormReset();
});


$(document).on('click', '#customer_order_btn', (e) => {
	let order_name  = $('#order_name').val();
	let door_width  = $('#door_width').val();
	let door_height = $('#door_height').val();
	let color       = $('#color option:selected').val();
	let side        = $('#side option:selected').val();
	let moves       = $('#moves option:selected').val();
	let client_id   = $('#client_id').val();

	let dataStr =  {
                    'order_name': order_name,
                    'door_width': door_width,
                	'door_height': door_height ,
                    'color': color,
                    'side': side,
                    'moves': moves,
                    'client_id': client_id 
            };

    if(order_name === "") {
        alert('Enter order name');
        $('#order_name').focus();
        return false;
    }
    else if(door_width === "") {
        alert('Enter door width');
        $('#door_width').focus();
        return false;
    }
    else if(door_height === "") {
        alert('Enter door height');
        $('#door_height').focus();
        return false;
    }
    else if(color === "") {
        alert('Select color');
        $('#color').eq(0).focus();
        return false;
    }
    else if(side === "") {
        alert('Select side');
        $('#side').eq(0).focus();
        return false;
    }
    else if(moves === "") {
        alert('Select move');
        $('#moves').eq(0).focus();
        return false;
    }
    else if(client_id === "") {
        alert('Client id required');
        return false;
    }
    else {
	    $.ajax({
		    type: 'POST',
		    url: '/create-order',
		    data: dataStr,
		    success:(data) => {
                if(data.flag == 1) {
                  console.log(`yes- ${data.flag}`);
                  $('#response_status').text(data.msg);
                  setTimeout(() => {
                     $('#response_status').text(''); 
                     orderFormReset();      
                  }, 1500);                 
                }
			    console.log(`resposen: ${JSON.stringify(data)}` );
		    }
	    });
    	console.log(`dataStr: ${dataStr}` );
   }
});

