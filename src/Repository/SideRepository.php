<?php

namespace App\Repository;

use App\Entity\Side;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Side|null find($id, $lockMode = null, $lockVersion = null)
 * @method Side|null findOneBy(array $criteria, array $orderBy = null)
 * @method Side[]    findAll()
 * @method Side[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SideRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Side::class);
    }

    public function countAll()
    {
        $db = $this->createQueryBuilder('s');
        return $db->select('count(s.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    // /**
    //  * @return Side[] Returns an array of Side objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Side
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
