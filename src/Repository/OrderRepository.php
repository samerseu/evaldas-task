<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function countAll($userId = null)
    {

        $db = $this->createQueryBuilder('o');

        if($userId)
           return $db->select('count(o.id)')
                ->where('o.client_id = :userId')
                ->setParameter("userId", $userId)
            ->getQuery()
            ->getSingleScalarResult();
        else 
             return $db->select('count(o.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAll($userId = null, $page = 1, $perPage) {
        $limit_num = (($page-1) * $perPage);
        $offset_num =  $perPage;

        // echo "from repository getall: ". $userId;
        $conn = $this->getEntityManager()
                    ->getConnection();
        if($userId) {
             $sql = "SELECT 
                            o.id, o.order_name, o.door_width, o.door_height, c.name color_name, 
                            s.name side_name, m.name move_name, u.email
                    FROM orders o
                    INNER JOIN color c ON (c.id = o.color_id)
                    INNER JOIN side  s ON (s.id = o.side_id)
                    INNER JOIN move  m ON (m.id = o.move_id)
                    INNER JOIN user  u ON (u.id = o.client_id)
                    WHERE o.client_id = :user_id 
                    ORDER BY o.id DESC 
                    LIMIT ".$limit_num ." , ". $offset_num;

            $stmt = $conn->prepare($sql);
            $stmt->execute(['user_id' => $userId]);
        }else {
            $sql = "SELECT 
                            o.id, o.order_name, o.door_width, o.door_height, c.name color_name, 
                            s.name side_name, m.name move_name, u.email
                    FROM orders o
                    INNER JOIN color c ON (c.id = o.color_id)
                    INNER JOIN side  s ON (s.id = o.side_id)
                    INNER JOIN move  m ON (m.id = o.move_id)
                    INNER JOIN user  u ON (u.id = o.client_id)
                    ORDER BY o.id DESC
                    LIMIT ".$limit_num ." , ". $offset_num;

            $stmt = $conn->prepare($sql);           
            $stmt->execute();
        }
       return $stmt->fetchAll();  
    }



    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
