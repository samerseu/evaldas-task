<?php

namespace App\DataFixtures;


use App\Entity\Color;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class ColorFixtures extends Fixture
{

    private $em;

    public function __construct( EntityManagerInterface $entityManager)
    {        
        $this->em = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $colorsData = [
              0 => [
                  'name' => 'Red'                  
              ],
               1 => [
                  'name' => 'Blue'                  
              ]
              , 
              2 => [
                   'name' => 'Green'
               ]
        ];

        foreach ($colorsData as $color) {
            $newColor = new Color();
            $newColor->setName($color['name']);            
            $this->em->persist($newColor);
        }

        $manager->flush();
    }
}
