<?php

namespace App\DataFixtures;


use App\Entity\Move;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class MoveFixtures extends Fixture
{

    private $em;

    public function __construct( EntityManagerInterface $entityManager)
    {        
        $this->em = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $movesData = [
              0 => [
                  'name' => 'Up'                  
              ],
               1 => [
                  'name' => 'Down'                  
              ],
               2 => [
                  'name' => 'Sideways'                  
              ]
        ];

        foreach ($movesData as $move) {
            $newMove = new Move();
            $newMove->setName($move['name']);            
            $this->em->persist($newMove);
        }

        $manager->flush();
    }
}
