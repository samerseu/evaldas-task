<?php

namespace App\DataFixtures;


use App\Entity\Color;
use App\Entity\Side;
use App\Entity\Order;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class OrderFixtures extends Fixture
{

    private $em;

    public function __construct( EntityManagerInterface $entityManager)
    {        
        $this->em = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $ordersData = [
              0 => [
                  'order_name' => 'Mr Abul Kashem',
                  'door_width' => 100,
                  'door_height' => 100,
                  'door_color_id' => 1,
                  'door_side_id' => 1,    
                    'door_move_id' => 2,    
                  'order_date' => new \DateTime(),
                  'client_id' => 1,
                  'order_status' => 1
              ]
        ];

        foreach ($ordersData as $order) {
            $newOrder = new Order();
            $newOrder->setOrderName($order['order_name']);            
            $newOrder->setDoorWidth($order['door_width']);            
            $newOrder->setDoorHeight($order['door_height']);            
            $newOrder->setDoorColorId($order['door_color_id']); 
            $newOrder->setDoorSideId($order['door_side_id']);            
            $newOrder->setDoorMoveId($order['door_move_id']);            
            $newOrder->setOrderDate($order['order_date']); 
            $newOrder->setClientId($order['client_id']);   
            $newOrder->setOrderStatus($order['order_status']);         
            $this->em->persist($newOrder);
        }

        $manager->flush();
    }
}
