<?php

namespace App\DataFixtures;


use App\Entity\Side;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class SideFixtures extends Fixture
{

    private $em;

    public function __construct( EntityManagerInterface $entityManager)
    {        
        $this->em = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $sidesData = [
              0 => [
                  'name' => 'Left'                  
              ],
               1 => [
                  'name' => 'Right'                  
              ]
        ];

        foreach ($sidesData as $side) {
            $newSide = new Side();
            $newSide->setName($side['name']);            
            $this->em->persist($newSide);
        }

        $manager->flush();
    }
}
