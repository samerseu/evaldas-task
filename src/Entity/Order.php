<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $order_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $door_width;

    /**
     * @ORM\Column(type="integer")
     */
    private $door_height;

    /**
     * @ORM\Column(type="integer")
     */
    private $color_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $side_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $move_id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $order_date;

    /**
     * @ORM\Column(type="integer")
     */
    private $client_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $order_status;
  


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderName(): ?string
    {
        return $this->order_name;
    }

    public function setOrderName(string $order_name): self
    {
        $this->order_name = $order_name;

        return $this;
    }

    public function getDoorWidth(): ?int
    {
        return $this->door_width;
    }

    public function setDoorWidth(int $door_width): self
    {
        $this->door_width = $door_width;

        return $this;
    }

    public function getDoorHeight(): ?int
    {
        return $this->door_height;
    }

    public function setDoorHeight(int $door_height): self
    {
        $this->door_height = $door_height;

        return $this;
    }

    public function getDoorColorId(): ?int
    {
        return $this->color_id;
    }

    public function setDoorColorId(int $door_color_id): self
    {
        $this->color_id = $door_color_id;

        return $this;
    }

    public function getDoorSideId(): ?int
    {
        return $this->side_id;
    }

    public function setDoorSideId(int $door_side_id): self
    {
        $this->side_id = $door_side_id;

        return $this;
    }

    public function getDoorMoveId(): ?int
    {
        return $this->move_id;
    }

    public function setDoorMoveId(int $door_move_id): self
    {
        $this->move_id = $door_move_id;

        return $this;
    }

    public function getOrderDate(): ?\DateTimeInterface
    {
        return $this->order_date;
    }

    public function setOrderDate(\DateTimeInterface $order_date): self
    {
        $this->order_date = $order_date;

        return $this;
    }

    public function getClientId(): ?int
    {
        return $this->client_id;
    }

    public function setClientId(int $client_id): self
    {
        $this->client_id = $client_id;

        return $this;
    }

    public function getOrderStatus(): ?int
    {
        return $this->order_status;
    }

    public function setOrderStatus(int $order_status): self
    {
        $this->order_status = $order_status;

        return $this;
    }

    
}
