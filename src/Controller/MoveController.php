<?php

namespace App\Controller;

use App\Repository\MoveRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MoveController extends AbstractController
{
    /**
     * @Route("/move", name="move")
     */
    public function index(MoveRepository $moveRepository): Response
    {
        $moves = $moveRepository->findAll();
        return $this->render('move/index.html.twig', [
            'controller_name' => 'MoveController',
            'moves' => $moves
        ]);
    }
}
