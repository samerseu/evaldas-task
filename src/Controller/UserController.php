<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     */
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();
        return $this->render('user/index.html.twig', [
            'controller_name' => 'Users',
            'users' => $users
        ]);
    }

    
    /**
     * @Route("/user", name="user")
     */
    public function create(): Response
    {        
        return $this->render('user/create.html.twig', [
            'controller_name' => 'Create User'
        ]);
    }


    /**
    * @Route("/create-user", name="user-save")
    */
    public function saveuser(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response {

            $user = new User();            
            if($request->isXmlHttpRequest()){
                
                $user->setEmail($request->request->get('email'));  
                // $user->setPassword($request->request->get('pass'));  
                $user->setRoles([ $request->request->get('role') ]);  
            
                $password = $passwordEncoder->encodePassword($user, $request->request->get('pass'));
                $user->setPassword($password);

              
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();  
                
          
                $res = new Response(json_encode(['msg'=> 'User Created successfully', 'flag'=> 1]));
                $res->headers->set('Content-Type', 'application/json');
                return $res;
            }
   }
}
