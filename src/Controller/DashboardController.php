<?php

namespace App\Controller;


use App\Repository\UserRepository;
use App\Repository\ColorRepository;
use App\Repository\SideRepository;
use App\Repository\MoveRepository;
use App\Repository\OrderRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DashboardController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
       $this->security = $security;
    }

    /**
     * @Route("/dashboard", name="dashboard")
     * @IsGranted("ROLE_USER")
     */
    public function index(ColorRepository $colorRepository,
        SideRepository $sideRepository,
        MoveRepository $moveRepository,
        OrderRepository $orderRepository,
        UserRepository $userRepository
    ): Response

    {
           $totalColor = $colorRepository->countAll();
           $totalSide  = $sideRepository->countAll();
           $totalMove  = $moveRepository->countAll();
           $totalUser  = $userRepository->countAll();

           // echo $this->security->getUser()->getRoles()[0];
           // exit();

           $totalOrder = $this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN' ? 
                            $orderRepository->countAll(null) : $orderRepository->countAll($this->security->getUser()->getId());

           // $totalOrder = $orderRepository->countAll();
        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
            'totalColor'      => $totalColor, 
            'totalSide'       => $totalSide,
            'totalMove'       => $totalMove,
            'totalOrder'      => $totalOrder,
            'totalUser'      => $totalUser
        ]);
    }


    

}
