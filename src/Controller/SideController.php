<?php

namespace App\Controller;


use App\Repository\SideRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SideController extends AbstractController
{
    /**
     * @Route("/side", name="side")
     */
    public function index(SideRepository $sideRepository): Response
    {
        $sides = $sideRepository->findAll();
        return $this->render('side/index.html.twig', [
            'controller_name' => 'SideController',
            'sides' => $sides
        ]);
    }
}
