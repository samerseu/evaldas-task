<?php

namespace App\Controller;

use App\Entity\Color;
use App\Repository\ColorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ColorController extends AbstractController
{
    /**
     * @Route("/color", name="color")
     */
    public function index(ColorRepository $colorRepository): Response
    {
        $colors = $colorRepository->findAll();
        $totalColor = $colorRepository->countAll();
        return $this->render('color/index.html.twig', [
            'controller_name' => 'ColorController',
            'colors' => $colors,
            'totalColor' => $totalColor
        ]);
    }
}
