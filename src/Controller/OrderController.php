<?php

namespace App\Controller;

use App\Entity\Order;
use App\Repository\ColorRepository;
use App\Repository\SideRepository;
use App\Repository\MoveRepository;
use App\Repository\OrderRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
       $this->security = $security;
    }

    /**
     * @Route("/order", name="order")
     */
    public function index(ColorRepository $colorRepository, SideRepository $sideRepository, MoveRepository $moveRepository): Response
    {
        $colors = $colorRepository->findAll();
        $sides = $sideRepository->findAll();
        $moves = $moveRepository->findAll();
        return $this->render('order/index.html.twig', [
            'controller_name' => 'Order Create',
            'colors' => $colors,
            'sides' => $sides,
            'moves' => $moves
        ]);
    }


     /**
     * @Route("/orders", name="orders")
     */
    public function list(OrderRepository $orderRepository, Request $request) :Response{ 
         $perPage = 5;
        $currentPage = empty($request->query->get('page')) ? 1 : $request->query->get('page');


        $orders = $this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN' ? $orderRepository->getAll(null, $currentPage, $perPage) : $orderRepository->getAll($this->security->getUser()->getId(), $currentPage, $perPage);
        $currentUserId = $this->security->getUser()->getId() ? $this->security->getUser()->getId() : null;

          $totalRecords = $orderRepository->countAll($this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN' ? null : $currentUserId);

         
        //echo "\r\n";
        $totalPage = ceil($totalRecords/$perPage);


        return $this->render('order/list.html.twig', [          
            'controller_name' => 'Customer Orders',            
            'orders' => $orders,
            'currentPage' => $currentPage,
            'totalRecords' => $totalRecords,
            'totalPage' => $totalPage,
            'perPage' => $perPage
        ]);



       
/*     
       //====== old code without pagination   
       $orders = $this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN' ? $orderRepository->getAll(null) : $orderRepository->getAll($this->security->getUser()->getId());
        $currentUserId = $this->security->getUser()->getId() ? $this->security->getUser()->getId() : null;

        // echo $currentUserId;        
        // echo "<pre>";
        // var_dump($orders);
        // echo "</pre>";
        // exit();

        return $this->render('order/list.html.twig', [
            'controller_name' => 'Customer Orders',            
            'orders' => $orders
        ]);*/
    }



    /**
     * @Route("/create-order", name="order-save")
     */
    public function saveOrder(Request $request): Response {

            $order = new Order();
            //$form = $this->createForm(OrderType::class, $order);
            //$form->handleRequest($request);
          //  if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
          // if ($request->isMethod('POST') ) {
            if($request->isXmlHttpRequest()){
                
                $order->setOrderName($request->request->get('order_name'));
                $order->setDoorWidth($request->request->get('door_width'));
                $order->setDoorHeight($request->request->get('door_height'));
                $order->setDoorSideId($request->request->get('side'));
                $order->setDoorColorId($request->request->get('color'));
                $order->setDoorMoveId($request->request->get('moves'));
                $order->setOrderDate(new \DateTime());
                $order->setClientId((int)$request->request->get('client_id'));
                $order->setOrderStatus(1);

                $em = $this->getDoctrine()->getManager();
                $em->persist($order);
                $em->flush();  
          
                $res = new Response(json_encode(['msg'=> 'Order Created successfully', 'flag'=> 1]));
                $res->headers->set('Content-Type', 'application/json');
                return $res;

                /*
                // json response
                $response = new Response(json_encode($request->request->all()));
                $response->headers->set('Content-Type', 'application/json');
                return $response;*/
                //$form->submit(array_merge($json, $request->request->all()));
            }
  
    }   
 

}
